var annotated_dup =
[
    [ "BNO055", null, [
      [ "BNO055", "classBNO055_1_1BNO055.html", "classBNO055_1_1BNO055" ]
    ] ],
    [ "ClosedLoop", null, [
      [ "ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "encoder_driver", null, [
      [ "Encoder", "classencoder__driver_1_1Encoder.html", "classencoder__driver_1_1Encoder" ]
    ] ],
    [ "motor_driver", null, [
      [ "Motor", "classmotor__driver_1_1Motor.html", "classmotor__driver_1_1Motor" ]
    ] ],
    [ "motordriver_l4", null, [
      [ "DRV8847", "classmotordriver__l4_1_1DRV8847.html", "classmotordriver__l4_1_1DRV8847" ],
      [ "Motor", "classmotordriver__l4_1_1Motor.html", "classmotordriver__l4_1_1Motor" ]
    ] ],
    [ "panel_driver", null, [
      [ "Panel_driver", "classpanel__driver_1_1Panel__driver.html", "classpanel__driver_1_1Panel__driver" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_controller", null, [
      [ "Task_controller", "classtask__controller_1_1Task__controller.html", "classtask__controller_1_1Task__controller" ]
    ] ],
    [ "task_encoder", null, [
      [ "Task_encoder", "classtask__encoder_1_1Task__encoder.html", "classtask__encoder_1_1Task__encoder" ]
    ] ],
    [ "Task_IMU", null, [
      [ "Task_IMU", "classTask__IMU_1_1Task__IMU.html", "classTask__IMU_1_1Task__IMU" ]
    ] ],
    [ "task_motor", null, [
      [ "Task_motor", "classtask__motor_1_1Task__motor.html", "classtask__motor_1_1Task__motor" ]
    ] ],
    [ "task_motor_l4", null, [
      [ "Task_motor", "classtask__motor__l4_1_1Task__motor.html", "classtask__motor__l4_1_1Task__motor" ]
    ] ],
    [ "task_panel", null, [
      [ "Task_panel", "classtask__panel_1_1Task__panel.html", null ]
    ] ],
    [ "task_user", null, [
      [ "Task_user", "classtask__user_1_1Task__user.html", "classtask__user_1_1Task__user" ]
    ] ],
    [ "task_user_l4", null, [
      [ "Task_user", "classtask__user__l4_1_1Task__user.html", "classtask__user__l4_1_1Task__user" ]
    ] ]
];