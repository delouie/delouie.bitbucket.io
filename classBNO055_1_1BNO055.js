var classBNO055_1_1BNO055 =
[
    [ "__init__", "classBNO055_1_1BNO055.html#adace37d9214a7ca2a1a02cd211f2c0c8", null ],
    [ "calib_read", "classBNO055_1_1BNO055.html#a3aed84816ea88c4d20562cfe1fdbde84", null ],
    [ "calib_status", "classBNO055_1_1BNO055.html#aca9a22e2d1ca740e7a25d4346a9d4fad", null ],
    [ "calib_write", "classBNO055_1_1BNO055.html#aa241cdf288cf0bd4a705f86494d2f658", null ],
    [ "euler_angle", "classBNO055_1_1BNO055.html#a330f07632dc2a1109ddee0b056cb6dce", null ],
    [ "euler_velocity", "classBNO055_1_1BNO055.html#a4cc257aea687a0c11869c933cc901195", null ],
    [ "operating_mode", "classBNO055_1_1BNO055.html#a8fbd146e6dbda03d8744bad09d9ff0b5", null ],
    [ "set_units", "classBNO055_1_1BNO055.html#a7551a5b702b66b121bde625ff581700d", null ],
    [ "buf", "classBNO055_1_1BNO055.html#a4cdc5f6519326ca1411218bb2f4aed56", null ],
    [ "buf_radius", "classBNO055_1_1BNO055.html#a0032d8ed0748b1d23fd89aa0ed797946", null ],
    [ "cal_bytes", "classBNO055_1_1BNO055.html#a8eb7387a5ecf01122a23a90d6137459e", null ],
    [ "i2c", "classBNO055_1_1BNO055.html#a8eebc88bf7ecfe69323da2b089911508", null ]
];