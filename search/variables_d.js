var searchData=
[
  ['ser_0',['ser',['../classtask__encoder_1_1Task__encoder.html#a9ad5b3202122190100d75612ba9caf53',1,'task_encoder.Task_encoder.ser()'],['../classtask__user_1_1Task__user.html#a5b83e097f09aad13cfa2c9d46ec65043',1,'task_user.Task_user.ser()']]],
  ['set_5fpoint_5fx_1',['set_point_x',['../classtask__user_1_1Task__user.html#ae2a2bb64870fff36a6941842defeabb3',1,'task_user::Task_user']]],
  ['set_5fpoint_5fy_2',['set_point_y',['../classtask__user_1_1Task__user.html#ab69687f3698ab777d398d988028ca56d',1,'task_user::Task_user']]],
  ['size_5fx_3',['size_x',['../classpanel__driver_1_1Panel__driver.html#a0cff42cf662d360c33faf9c764463259',1,'panel_driver::Panel_driver']]],
  ['size_5fy_4',['size_y',['../classpanel__driver_1_1Panel__driver.html#a2f9ae70fa306c46025492c662de9feb4',1,'panel_driver::Panel_driver']]],
  ['starttime_5',['starttime',['../classtask__encoder_1_1Task__encoder.html#ae265fb94d9b5971420e124b90be0eb47',1,'task_encoder.Task_encoder.starttime()'],['../classtask__motor__l4_1_1Task__motor.html#af936820b51473753f68aa26592094829',1,'task_motor_l4.Task_motor.starttime()'],['../main__l2_8py.html#afe20acb3f72c8907dcb953a51506dcc1',1,'main_l2.starttime()']]],
  ['starttimec_6',['starttimec',['../classtask__encoder_1_1Task__encoder.html#a68ca757e9f77d15e3f6c696b28949872',1,'task_encoder.Task_encoder.starttimec()'],['../classtask__motor__l4_1_1Task__motor.html#a4b84c33c26b7266dd44f756dedc974f0',1,'task_motor_l4.Task_motor.starttimec()']]]
];
